import {appendTweets, getTweets, postTweet} from "./tweets-service.js";

window.onload = async function () {
  const tweetDom = document.getElementById("tweets");

  const tweets = await getTweets();
  appendTweets(tweetDom, tweets)

  document.getElementById("tweetbtn").addEventListener("click", async function() {
    await postTweet();
    tweetDom.innerHTML = null;
    const tweets = await getTweets();
    appendTweets(tweetDom, tweets)
  });
};