package main

import (
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/ziggornif/gitlab-docker-example/tweet"
)

func main() {
	router := gin.Default()
	router.Use(cors.Default())

	tweetService := tweet.NewTweetService()

	router.GET("/tweets", func(c *gin.Context) {
		tweets := tweetService.ListTweets()
		c.JSON(http.StatusOK, tweets)
	})

	router.POST("/tweets", func(c *gin.Context) {
		var input tweet.TweetRequest
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		createdTweet := tweetService.CreateTweet(input)

		c.JSON(http.StatusOK, createdTweet)
	})

	router.POST("/tweets/:id/likes", func(c *gin.Context) {
		tweetID := c.Param("id")
		likeErr := tweetService.LikeTweet(tweetID)
		if likeErr != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": likeErr.Error()})
			return
		}

		c.Status(http.StatusNoContent)
	})

	router.Static("/public", "./public")

	router.LoadHTMLFiles("./public/index.html")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	err := router.Run(":8080")
	if err != nil {
		log.Fatalf("Error while running app... (cause: %v)", err.Error())
	}
}
