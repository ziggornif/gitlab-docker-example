package tweet

import (
	"errors"
	"time"

	"github.com/google/uuid"
)

type Tweet struct {
	ID        string
	CreatedAt time.Time
	Message   string
	Author    string
	Likes     int64
}

func (t *Tweet) ToResponse() TweetResponse {
	return TweetResponse{
		ID:        t.ID,
		Message:   t.Message,
		CreatedAt: t.CreatedAt,
		Author:    t.Author,
		Likes:     t.Likes,
	}
}

type TweetRequest struct {
	Message string `json:"message"`
	Author  string `json:"author"`
}

func (t *TweetRequest) NewTweet() Tweet {
	id, _ := uuid.NewUUID()
	return Tweet{
		ID:        id.String(),
		CreatedAt: time.Now(),
		Message:   t.Message,
		Author:    t.Author,
	}
}

type TweetResponse struct {
	ID        string    `json:"id"`
	Message   string    `json:"message"`
	CreatedAt time.Time `json:"created_at"`
	Author    string    `json:"author"`
	Likes     int64     `json:"likes"`
}

type tweetService struct {
	tweets []Tweet
}

type TweetService interface {
	ListTweets() []TweetResponse
	CreateTweet(request TweetRequest) TweetResponse
	LikeTweet(tweetID string) error
}

func NewTweetService() TweetService {
	tweets := make([]Tweet, 0)
	return &tweetService{
		tweets,
	}
}

func (ts *tweetService) findIndex(tweetID string) int {
	for index, tweet := range ts.tweets {
		if tweet.ID == tweetID {
			return index
		}
	}
	return -1
}

func (ts *tweetService) ListTweets() []TweetResponse {
	results := []TweetResponse{}
	for _, tweet := range ts.tweets {
		results = append(results, tweet.ToResponse())
	}
	return results
}

func (ts *tweetService) CreateTweet(request TweetRequest) TweetResponse {
	tweet := request.NewTweet()
	ts.tweets = append(ts.tweets, tweet)
	return tweet.ToResponse()
}

func (ts *tweetService) LikeTweet(tweetID string) error {
	index := ts.findIndex(tweetID)
	if index == -1 {
		return errors.New("tweet not found")
	}
	ts.tweets[index].Likes += 1
	return nil
}
