MAIN=main.go
APP_NAME=gitlab-docker-example

build:
	go build -ldflags "-w -s" -o ${APP_NAME} $(MAIN)

unused:
	go mod tidy

run:
	./${APP_NAME}

