FROM golang:1-alpine as builder
RUN apk update && apk add gcc make g++
WORKDIR /build
ADD . .
RUN make build

FROM alpine
COPY --from=builder /build/gitlab-docker-example /bin/gitlab-docker-example
RUN chmod +x /bin/gitlab-docker-example

# Add static content
COPY --from=builder /build/public ./public

ENV GIN_MODE=release

ENTRYPOINT ["/bin/gitlab-docker-example"]
